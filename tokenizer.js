const process = require("process");
const fs = require("fs");
const path = require("path");

const symbols = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '>', '<', '=', '~',];
const keywords =
  ['class', 'constructor', 'function', 'method', 'field', 'static', 'var', 'int', 'char', 'boolean',
    'void', 'true', 'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while', 'return'];
const integerMax = 32767;
/**
 * symbol
 * keyword
 * integerConstant 0...32767
 * StringConstant "..."
 * identifier - sequence of letters, digits, underscore, not starting with number
 */

const tokenTypes = {
  symbol: "symbol",
  keyword: "keyword",
  integerConstant: "integerConstant",
  StringConstant: "StringConstant",
  identifier: "identifier"
};

const buildEnumerator = source => {
  let idx = -1;
  const enumerator = {
    hasNext: true,
    advance: () => {
      if (!enumerator.hasNext) {
        return;
      }
      idx += 1;
      enumerator.hasNext = idx < source.length;
      enumerator.curr = source[idx];
      enumerator.prev = source[idx - 1];
      enumerator.next = source[idx + 1];
    }
  };
  enumerator.advance();
  return enumerator;
}

const tokenize = source => {

  const tokens = [];
  const produceToken = (value, type) => {
    tokens.push({ value, type });
  }

  const enumerator = buildEnumerator(source);

  while (enumerator.hasNext) {

    const readWord = () => {
      let word = "";
      do {
        word += enumerator.curr;
        enumerator.advance();
      } while (enumerator.curr && /[A-Za-z0-9_]/.test(enumerator.curr));

      if (keywords.indexOf(word) != -1) {
        produceToken(word, tokenTypes.keyword);
      } else {
        produceToken(word, tokenTypes.identifier);
      }
    };

    const readStringConstant = () => {
      let stringConstant = "";

      // skip first "
      enumerator.advance();

      while (enumerator.curr != '"' && enumerator.hasNext) {
        stringConstant += enumerator.curr;
        enumerator.advance();
      }
      
      // skip last '"'
      enumerator.advance();

      produceToken(stringConstant, tokenTypes.StringConstant);
    };

    const skipComments = () => {
      if (enumerator.next === '/') {
        while (enumerator.curr != '\n' && enumerator.hasNext) {
          enumerator.advance();
        }
      }
      if (enumerator.next === '*') {
        do {
          enumerator.advance();
        } while (!(enumerator.prev == '*' && enumerator.curr == '/') && enumerator.hasNext);
        enumerator.advance();
      }
    };

    const readNumber = () => {
      let numberString = '';
      do {
        numberString += enumerator.curr;
        enumerator.advance();
      } while (/\d/.test(enumerator.curr));
      produceToken(numberString, tokenTypes.integerConstant)
    };

    const produceSymbol = () => {
      produceToken(enumerator.curr, tokenTypes.symbol);
      enumerator.advance();
    };

    if (enumerator.curr === '/') {
      if (enumerator.next === '/' || enumerator.next === '*') {
        skipComments();
      } else {
        produceSymbol();
      }
    }
    else if (enumerator.curr === '"') {
      readStringConstant();
    }
    else if (/\s/.test(enumerator.curr)) {
      enumerator.advance();
    }
    else if (/\d/.test(enumerator.curr)) {
      readNumber();
    }
    else if (/[a-zA-Z_]/.test(enumerator.curr)) {
      readWord();
    }
    else if (symbols.indexOf(enumerator.curr) != -1) {
      produceSymbol();
    }
    else {
      console.error("untokenizable character", enumerator.prev, enumerator.curr, enumerator.next, idx);
    }
  }

  return tokens;
};

const writeTokens = (tokens, outputPath) => {
  fs.writeFile(outputPath, 
    ("<tokens>\n" +
    tokens.map(token => `<${token.typeString}> ${token.value} </${token.typeString}>`).join("\n") +
    "\n<\\tokens>"));
};

const outputPath = inputPath => {
  const fname = path.basename(inputPath);
  const dotSplittedFname = fname.split(".");
  return path.join(path.dirname(inputPath),
    dotSplittedFname.slice(0, dotSplittedFname.length - 1).join('.') + ".result.xml");
}

const tokenizeFileAtPath = fpath => {
  const tokens = tokenize(fs.readFileSync(fpath).toString());
  writeTokens(tokens, outputPath(fpath));
  console.log("xml wrote to", outputPath(fpath));
  return tokens;
}

if (typeof require != 'undefined' && require.main==module) {
  let argv = process.argv.slice(process.argv.indexOf(__filename) + 1, process.argv.length);
  let fnameOrDirname = argv[0];

  const fstat = fs.statSync(fnameOrDirname);
  if (fstat.isDirectory()) {
    fs.readdirSync(fnameOrDirname)
      .map(fname => path.join(fnameOrDirname, fname))
      .forEach(tokenizeFileAtPath);
  } else {
    tokenizeFileAtPath(fnameOrDirname);
  }
}


module.exports = {
  tokenizeFileAtPath,
  tokenize
}
