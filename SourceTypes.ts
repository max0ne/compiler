import { Token } from "./Token";


export interface ClassVarDec {
  staticness: Token,
  type: Token,
  varname: Token
}

export interface SubroutineDec {
  subroutineType: Token,
  returnType: Token,
  name: Token,
  parameterList: ParameterDec[],
  body: SubroutineBodyDec
}

export interface SubroutineBodyDec {
  varDecs: VarDec[],
  statements: StatementDec[]
}

export interface ClassDec {
  className: Token,
  classVarDecs: ClassVarDec[],
  subroutineDecs: SubroutineDec[]
}

export interface ParameterDec {
  type: Token,
  varName: Token
}

export interface VarDec {
  type: Token,
  varNames: Token[]
}

export interface StatementDec {

}

export const PrimitiveTypes = ['int', 'char', 'boolean'];
export const SubroutineTypes = ['constructor', 'function', 'method'];
export const StaticnessTypes = ['static', 'field'];
