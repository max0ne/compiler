import * as fs from 'fs';
import * as path from 'path';

import { Token, Type as TokenType } from "./Token";
import { Iterator } from "./TokenIterator";

import {
  ClassVarDec,
  SubroutineDec,
  SubroutineBodyDec,
  ClassDec,
  ParameterDec,
  VarDec,
  StatementDec,
  PrimitiveTypes,
  SubroutineTypes,
  StaticnessTypes
} from "./SourceTypes";

function parseTokens(tokens: Token[]) {
  const iter = new Iterator(tokens);

  const isType = (token: Token) => {
    return token.type == TokenType.identifier || (token.type == TokenType.keyword && PrimitiveTypes.indexOf(token.value) != -1);
  }

  const isReturnType = (token: Token) => {
    return isType(token) || token.value == 'void';
  };

  const isIdentifier = (token: Token) => token.type == TokenType.identifier;

  const throwUp = () => { throw iter.currentContext(10); };

  function scanSubroutineDecs(): SubroutineDec[] {
    const subroutines = [] as SubroutineDec[];
    while (iter.curr.value != '}') {
      subroutines.push(scanSubroutineDec());
    }
    return subroutines;
  }

  function scanClassVarDecs(): ClassVarDec[] {
    const classVarDecs = [] as ClassVarDec[];
    while (StaticnessTypes.indexOf(iter.curr.value) != -1) {
      classVarDecs.push(scanClassVarDec());
    }
    return classVarDecs;
  }

  function scanClassVarDec(): ClassVarDec {
    const staticness = iter.curr;
    scanString(StaticnessTypes);

    const type = scanType();
    const varname = scanIdentifier();
    scanSemiColon();

    return { staticness, type, varname };
  }

  function scanSubroutineDec(): SubroutineDec {

    // ('constructor' | 'function' | 'method')
    scanString(SubroutineTypes);
    const subroutineType = iter.curr;
    iter.advance();

    // ('void' | *type*) 
    if (!isReturnType(iter.curr)) {
      throw ['unexpected token', iter.curr.type, iter.curr.value].join(" - ");
    }
    const returnType = iter.curr;
    iter.advance();
    
    // subroutineName
    const name = scanIdentifier();

    // parameterList
    const parameterList = scanParameterList();

    // subroutineBody
    const body = scanSubroutineBody();

    return { subroutineType, returnType, name, parameterList, body };
  }

  function scanSemiColon() {
    scanString(";");
  }

  function scanSubroutineBody(): SubroutineBodyDec {
    scanString(["{"]);
    const varDecs = scanVarDecs();
    const statements = scanStatements();
    scanString(["}"]);
    return { varDecs, statements };
  }

  function scanParameter(): ParameterDec {
    let typeDec, nameDec: Token;

    typeDec = scanType();
    if (isIdentifier(iter.curr)) {
      nameDec = iter.curr;
      iter.advance();
    } else {
      throw 'ha';
    }

    const dec = {} as ParameterDec;
    dec.type = typeDec;
    dec.varName = nameDec;
    return dec;
  }

  function scanParameterList(): ParameterDec[] {
    // '('
    scanString('(');

    // parameters
    const parametersDec = [] as ParameterDec[];
    while (iter.curr.value != ')') {
      parametersDec.push(scanParameter());
      scanString(',');
    }

    // )
    iter.advance();

    return parametersDec;
  }

  function scanType(): Token {
    iter.advance()
    if (isType(iter.prev)) {
      return iter.prev;
    } else {
      throw ['unexpected token', iter.curr.type, iter.curr.value].join(" - ");
    }
  }

  function scanString(ss: string | string[]) {
    if (typeof(ss) === 'string') {
      iter.curr.value ? iter.advance() : throwUp();
    }
    else {
      ss.indexOf(iter.curr.value) != -1 ? iter.advance() : throwUp();
    }
  }

  function scanIdentifier(): Token {
    iter.advance()
    if (iter.prev.type == TokenType.identifier) {
      return iter.prev;
    } else {
      throw ['unexpected token', iter.curr.type, iter.curr.value].join(" - ");
    }
  }

  /**
   * var type varName(, varName)*;
   */
  function scanVarDec(): VarDec {

    // type
    const type = scanType();

    // first name
    const varNames = [scanIdentifier()];

    while (iter.curr.value != ';') {

      // ,
      if (!scanString(',')) {
        throwUp();
      }

      // more names
      varNames.push(scanIdentifier());
    }

    // ;
    scanSemiColon();

    return { type, varNames };
  }

  function scanVarDecs(): VarDec[] {
    const varDecs = [] as VarDec[];
    while (iter.curr.value === 'var') {
      iter.advance();
      varDecs.push(scanVarDec());
    }
    return varDecs;
  }

  function scanStatements(): StatementDec[] {
    return [] as StatementDec[];
  }

  function scanClass(): ClassDec | null {

    // class
    scanString('class');

    // className
    const className = scanIdentifier();

    // {
    scanString('{');

    // vars
    const classVarDecs = scanClassVarDecs();

    // subroutines
    const subroutineDecs = scanSubroutineDecs();

    // }
    scanString('}');

    return { className, classVarDecs, subroutineDecs };
  }

  return scanClass();
}



import { tokenizeFileAtPath } from "./tokenizer";

if (typeof require != 'undefined' && require.main == module) {

  const testFilePath = "/Users/maxpro/Projects/nand2tetris/projects/10/Square/Main.jack";

  let tokens: Token[] = tokenizeFileAtPath(testFilePath).map(raw => {
    return {
      type: (<any>{
        "symbol": TokenType.symbol,
        "keyword": TokenType.keyword,
        "integerConstant": TokenType.integerConstant,
        "StringConstant": TokenType.StringConstant,
        "identifier": TokenType.identifier
      })[raw.type] as TokenType,
      value: raw.value
    };
  });
  console.log(parseTokens(tokens));
}
