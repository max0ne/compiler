import { Token } from "./Token";

export class Iterator {
  readonly tokens: Token[];
  idx: number;

  hasNext: boolean;
  prev: Token;
  curr: Token;
  next: Token;

  constructor(tokens: Token[]) {
    this.tokens = tokens;
    this.hasNext = true;
    this.idx = -1;

    this.advance();

    this.currentContext = this.currentContext.bind(this);
    this.advance = this.advance.bind(this);
  }

  advance() {
    if (!this.hasNext) {
      return;
    }

    this.idx += 1;

    this.hasNext = this.idx < this.tokens.length;
    this.prev = this.tokens[this.idx - 1];
    this.curr = this.tokens[this.idx];
    this.next = this.tokens[this.idx + 1];
  }

  currentContext(numberOfTokens: number | undefined) {
    if (!numberOfTokens) {
      numberOfTokens = 10;
    }

    const tokens: string[] = [];
    for (var idx = Math.max(0, this.idx - numberOfTokens); idx < Math.min(this.tokens.length, this.idx + numberOfTokens); idx++) {
      if (idx == this.idx) {
        tokens.push('>>', this.tokens[idx].value, '<<');
      } else {
        tokens.push(this.tokens[idx].value);
      }
    }

    return tokens.join(" ");
  }
}
