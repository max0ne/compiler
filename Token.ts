const enum Type {
  /**
   * '{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '>', '<', '=', '~'
   */
  symbol,

  /**
   * ['class', 'constructor', 'function', 'method', 'field', 'static', 'var', 'int', 'char', 'boolean',
   * 'void', 'true', 'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while', 'return'];
   */
  keyword,

  /**
   * 0...32767
   */
  integerConstant,

  /**
   * "........"
   */
  StringConstant,

  /**
   * sequence of letters, digits, underscore, not starting with number
   */
  identifier,
};

interface Token {
  type: Type,
  value: string
};

export { Type, Token };
